package br.pucpr.cg;

import br.pucpr.mage.Mesh;
import br.pucpr.mage.MeshBuilder;
import org.joml.Vector3f;

public class MeshFactory {


    public static Mesh createPentagon(){
        return new MeshBuilder()
                .addVector3fAttribute("aPosition",
                        0.00f,  0.4f, 0.0f,//1
                        -0.50f,  0.00f, 0.0f,//2
                        0.50f,  0.00f, 0.0f,//3
                        -0.25f, -0.60f,0.0f,//4
                        0.25f, -0.60f,0.0f)//5
                .addVector3fAttribute("aColor",
                        1.0f, 0.0f, 0.0f,
                        1.0f, 1.0f, 1.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 0.0f, 0.5f)
                .setIndexBuffer(
                        0, 1, 2,
                        3, 2, 1,
                        3, 4, 2)
                .loadShader("/br/pucpr/resource/basic")
                .create();
    }

}
